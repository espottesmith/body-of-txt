Cate Blanchett


The winds are blazed
And bifurcating,
Too hot like Bolivian sun,
And dry.

Sky emblazoned with battlefield flags,
Clouds burgandy-dyed and bloodbathed.

In lakes there bubble the clear-blue
Light-struck waters,
The inlets bloodied with 
Fresh lambs freshly culled.

You climb
Again over braceless soil beds,
Biting the inside of a mask.
Couldn't stand her body.
The air is barbed and dangerous,
Thick with self-indulgent bursts
The black ash masturbating.

Here were the blackberry bushes,
In the woods where you saw yourself
Cate Blanchett,
Here the hollow tree
Where you spoke to bees,
That lie dumb today,
And may buzz nevermore.

How can we come back,
We sob,
You cry,
Brace ourselves against the batholith
And once again
Build monuments to long-bladed men?