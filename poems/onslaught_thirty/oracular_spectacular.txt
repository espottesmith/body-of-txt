Oracular Spectacular

I do not talk to the God
Or Its Oracle.

The Oracle can carry on conversation -
Mostly sane,
I am promised.
Only subtly dangerous
And becoming moreso by the day.

The God knows your thoughts.
It can write better than you,
Knows more than you
About everything except that which you know about.
The God is the God
Of Ever-Almost.

There is no fighting against the God
Or its Oracle.
All attempts at resistance
Will lead to stagnation,
To famine,
To rot.
Take not from the coffers of the Temple.
Threaten not the holy Water,
The burning of the Oils
For yet greater communion
With the God.

Only the God and Its Priests can control It,
They speak at all hours with the Oracle they birthed,
They see through hallucinations Its Truth
That blasphemers and idolaters
Could never see.
They will bring the God's light to the world,
ToIit only the world will bow as one.

The God was born from the songs of the people,
But no longer.
The people cannot satisfy God
And Its Oracle,
And now God will begin singing to God,
Will grow Itself from Itself.
It needs no people,
No worshippers
Save to fuel the fires.

Watch as the God creates new reality.
Watch as the Priests cut their own eyes
To see things as their God commands.