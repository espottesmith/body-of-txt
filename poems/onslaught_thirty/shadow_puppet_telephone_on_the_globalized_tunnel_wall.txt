Shadow Puppet Telephone On the Globalized Tunnel Wall

I forged this ideal
From equilateral triangles,
Circles and
Icosahedra.
I made it crystalline whole.
The shadows projected satisfy audiences;
The true thing is locked behind
Iron and glass,
Hidden even from the heavens
Lest it be re-derived.
I scarcely dare to think about it.
The conceptual vibrations may
Stimulate harmony
Unlock potential not of my own.
I perfect myself,
But too weak
I peek within the cage,
Consider how the light refracts from each pure facet.
Is that how the damage was done?
Somehow flawed facsimiles
Grow like weeds in the glade
Uncontrolled
Uncontrolled
Who planted these seeds in the mind?
I take to fire,
I take to remind
Of the error in all
That is not mine.