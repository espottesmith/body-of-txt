Advice from the Sages

One she
Sitting cross-legged
On a lecturer's table
In a classroom in Hamilton Hall
So as not to show me
Too much
(We would kiss
Once
Months later,
Sloppy and careless
After a party had ended,
And unremarkably)
Leaned over me,
As I sat nervous
In a student's desk,
Old carved wood
Touching the backs of
My thighs
On the first night I wore a mini-skirt out.

Paintbrush of lipstick in her hand,
I could smell her
Dirty-blonde ringlets
Just inches from my face.

Two of them -
Friends for the most part,
Experienced women,
Had promised to make
A character of me,
To help me perform
A part.
I was to be
A god(dess) of sorts,
A vain and terrible creature.
I needed something
That one could call drag.

As they contoured
And sculpted
A fresh fate
Out of powder and my
Old skin,
They advised me on
The issue of my legs.

I learned a trick
From the craftswomen,
Their arms covered by
Blazers,
Their legs by black stockings -
"Don't bother with shaving cream",
They said,
"Just use conditioner.
Even the cheaper ones are fine,"

"The blade will glide over you
All the same",
And though I have cut myself
Twice
And bled out onto the floor of my bathtub,
I have found
They were right.

"Just shave below your knee,"
The round-faced one told me
(We would later go on one date,
Dinner in my apartment,
But I - 
Lost and blissfully swimming - 
Did not realize
Her intentions for
Me and the
Night).
The sharp-toothed woman above me
Who had asked me to
Close my eyes and 
Purse my lips
Agreed,
"No one is worth shaving your thighs for."

She didn't say "No one",
Actually - 
I believe it was
"No man" -

But as I now stand,
Years and valleys away from the fluorescent lights
In Upper Manhattan,
In the shower,
My flesh clinging
To rivulets
As if everything
Might too soon slip away,
I wonder
"What about me?"