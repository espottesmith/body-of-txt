Intentional Hands

I was the one with quiet questions,
And you were the one with intentional hands,
Hands that were comfortable in their ownership,
Holding, directing monadic knowledge with force,
Planting needs.