To Return to Soft Wantings

My hands and lips
Are
Caught in
The frisson,
Snared in spider-silk

Will you pull me loose
Or
Will you
Wrap me tighter?

How wild,
My wants so
Wide
So child-sized

Is this
Yearning?

Jolting at the smallest touch

I
Yield
In the springs of your embraces,
Unbreathing to preserve this
Gossamer charm

Would you believe my ghosts
If they claimed
That my wants would be exhausted
With your heartbeat
In my chest,
Our palms' sweat
Indistinguishable?

Please do

My lies
So sweet
Can coat the teeth
Of us both