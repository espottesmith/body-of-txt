Our Bed

In our bed,
Where we showed each other
And ourselves
And found a peace
Innocent in our arms
And unsettling to the eyes
That found us,
Clothed as we were in all array,
There I can king the word.

Was it you who first started spotting the holes?
Was it I that filled them,
Fastidiously,
With putty and clay
And anything else that can be streteched and molded,
Like truths
And affections
And smiles?
It must have been, at first.
At least at first.

We came to acknowledge the faults
In what we could be
Through silences and glances
Accumulating in the spaces where we were coupled,
In the unlocked rooms and open lanes
And whispering parties
That always grew too loud.

We can laugh about it now,
Whether together or alone,
Or separate, if not alone.
The sound is sweetest when we do not even want words,
Only each other.
What language is needed in the agreement of our actions,
Our peaceful protest?