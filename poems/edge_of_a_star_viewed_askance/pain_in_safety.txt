Pain In Safety

From the windows
The world rushes by
Dying

Children scream in the streets
Parents having made their choice
At the altar of the Bills
They'll be sacrifices
And they'll be
Knife-in-the-throat

Their screams are joyful
And immodest

Birds still come by
Everyday

Their numbers dwindle
But the faithful still look for our seeds
I'm sorry
I try to tell them
The rats came too close
Too hundry

We cannot tolerate more vectors in our midst

We seldom even open the door
No friends come knocking
Lovers stay away
Respectfully
As we told them to
Some go out to bars
Some close their eyes
Pretend that Death is a campfire story
That Disability is a myth
Something that only happens to other people
The Unlucky Ones

I am frozen
Untouched
Watching them come and fly away
And fall
As the dying world goes by