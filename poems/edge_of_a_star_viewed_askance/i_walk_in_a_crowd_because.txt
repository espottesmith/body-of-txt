I Walk In a Crowd, Because

Some die if we do,
Die if we don't.

It's how these things too often go.

But I go this way,
Because this way,
I could be among the ones to die,

Because this way,
No one could dare to ask
Why?