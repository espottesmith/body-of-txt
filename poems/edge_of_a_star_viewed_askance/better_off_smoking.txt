Better Off Smoking

I cannot hold all these hours
in my arms,
I cannot cross this threshold
without fear
that I may not come back,
that I
will not be the same
when I walk back over
a second time.
I seize.
I am filled with hot tar
and quick-dry cement.
Each breath that I hear
Cuts five minutes from my life.
I would be better off smoking.