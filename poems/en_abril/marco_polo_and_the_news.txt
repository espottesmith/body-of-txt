Marco Polo and the News

Did you hear the news?
The laws and the
Bigotry?

I'm grateful,
Really I am.
New dry wood
For the spiraling.

You always need
A death to fear,
And the old plague had lost its edge
Years ago
When we let the old and ill
Crippled bodies
Die in dozens
Of thousands.

It smells of
Segregation
And the Death Camps
With firing squads
In the distance as the clouds grow thick
And menacing.

But here and now
We're pelted with
The kids
Slitting wrists
They can't get their pills
Cause it's child abuse
To let humans
Love
Themselves.

There are registers
And police raids
Because that trick
Never seems
To age.
The bullets and the chokeholds
From a shining badge
Of lies.

I wake up
Nearly every day
Checking the news 
While I'm still in bed.
I plan escape routes
In my mind.

What about you?
Do you play the game?
Will you say "Marco"
Then run away?
Will you hide until
They find you
Shaking
But
Still warm?

Anyway,
I hope you have
A lovely day,
Hope you fall asleep
Quickly
And don't worry
All that much.