Regression

Pushing down,
Compressing
Reducing.
Triangle-wedge flight-mark
Score line
Press in.

We had to learn
To add on,
To paint on top of,
Operating on the edge
Of two dimensions.

Illuminated manuscripts,
Scratchwork and artistry
Of inks and dyes,

And where are we now?
Fully flat.
The array of pixels displayeds on a screen
Is fixed,
And yet
The art is washed away
With every shutdown
And power surge.

We find ourselves
In the era of
Stasis,
Kept here by
Running in place.