Measured Attention

You worried
That with the crawling of the Spring
We would not be able
To see the birds
Fighting
And courting
And living in our tree,
But consider:
Now we have reason
To watch all the more intensely