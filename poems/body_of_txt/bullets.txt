Bullets

In war
Bullets fly wide.
Ninety-eight per cent of soldiers
Will intentionally miss their targets,
Usually aiming high
Above the head.
The other two per cent
Are sociopaths.

We can up those numbers,
And we have.
