Florida Rain

The Sun is out
On the Florida beach,

The clouds scattered moments
That alone form a chorus,
Rain dropping bodies upon me,
That fill my lungs and choke me,
The lightning cracking but
It will stop soon.

It will stop soon,
Your feet yell out over
The rage of Heaven,
Run to the car
And hide for the moment.
It will only be a moment.

But the water
Breaks stone
And it threatens the car -
It will end soon -
But will the tires
Forged in fire,
Vulcanized, volcano-made,
Will they stop the lightning
If it turns to me?

I do not know.
Turn the ignition,
Make fire in the car,
Run away
But blast the Florida
Beach-town stations,
Turn the volume so high
That you can make out
The melody
But not the words in the Florida rain.
Sing along - 
It will stop soon.

The water fights barriers with
Bloody fingernails,
Finds cracks -
The Florida rain is coming in.
Put the towel,
Already soaking,
To the window,
Stop the flood from reaching you.
You're asked quietly so you can barely hear
Over the radio and the
Hate that gravity brings,
Will it stop soon?

Drive,
Drive though you can't see
Through the Florida rain,
Though it blinds you
And deafens you,
If the radio -
Still singing as if it had stopped -
Does not deafen you first,
Drive to a place you cannot recognize.

Drive to anywhere where it's stopped.
Go faster than the flood
That now you name,
That now you see,
Slow so you do not
Get caught in it,
Slip in the water and lose
What little control
The storm will yield to you.

It goes dark,
The only light the flashing
Sky-breaker,
The beacon of destruction.
You cannot even ask of this Florida rain -
No, it will not stop,
It cannot stop.
It will never stop again.

Drive,
You are as alone as me
In the Florida rain that should have stopped.
You need to
Drive
And roll the windows down,
Why don't you?
No part of the car is untouched,
You're as cold and as seized
As if you ran against the wind,
You don't even care,
But you drive,
You curse the storm
And the voice on the radio that tells you
To avoid the roads,
That they're unsafe.

Just tell me when it will stop,
You curse,
You're shivering in the Florida rain
That should have been a warm rain.
And you don't know if it's from the cold
Or from the fear
Of swerving off of this bridge
And not being seen
Or heard over
The yelling of
The feuding gods.
You're shaking,
And you're swearing,
And you're begging that it stop.

I'm almost home,
You say, but you can
Barely remember where home is,
You can't even hear
The Florida radio
Telling you that it will stop.
If you wait then it must stop.
Just stay where you are,
Be inside where it's warm,
It will stop -

Stop the car.
Get out and step out
Into the rain.
Shiver and screan because
You know
No one will hear you
In the Florida rain.
Scream in the fire and the ice,
Scream at the faces of the gods,
Shiver and lose all feeling.

Let it happen -
It will stop - 
Let it happen - 
And it stops -
Let it happen -
It's stopped.