A Small Consolation

The angels tread calmly
Upon the most turbulent waters,
Waves within waves
Propagating chaos,
Or so the mortal eye sees.

You do not need to solve an equation to be.

The eyes of the armchair god
Recede from the apparent,
And the blur conjures shadows
Of the shapes of old doubts.
In the unfocused space
Turn the turbulent waters,
The waves
With their gingerly folded infinities,
Touched at the corners and cusps.
The angels sound trumpets, scream
"Fear not",
But too soon
The placid observer is taken.
Hard cells close gray spaces
And impossible
Jail-break hoppings disturb even
The constancy of their panic.

The only
Consolation to this anguished god:

You don't need an equation to be.