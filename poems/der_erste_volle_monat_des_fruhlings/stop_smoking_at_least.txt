Stop Smoking, At Least

Do you have to be here?
	While you're with them,
	Yes.

But do you have to smoke?
It's bad enough with
The cats
Making me fear for my life.

	Why do you care?
	You can be distracted
	So easily,
	Even with them
	In the room?

To be honest,
I didn't even notice you
At first,
But now I do
And you're the largest thing
In the room.
	I always am,
	Darling.
	I always -
But always means -
	Before and after?
	But of course;
	You needed me
	When your need for them
	Was distant.

And after?
Will they be dead,
Or I,

	If and when,
	Child,
	You no longer feel
	That giddiness and
	Peace upon homecoming,
	I will still be there.
	Hate is a type of want, too.

Fuck off,
For now.
Sit there and smoke.
I'll let you fill my lungs,
This once.

	Don't try to deny me
	My home.
	I have a tendency
	To burn.