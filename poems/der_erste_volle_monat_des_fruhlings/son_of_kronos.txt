Son of Kronos

You were once
A son of Kronos,
But uncounted,
Unsaved,
Not one of the lucky ones.

You were eaten like veal,
Young and unfit for this world.
Your fresh blood unremarkable,
Unremarked upon.

You were eaten,
Your body unrecovered.
Eaten
And shat out onto the cold earth,
The same spot where your blood spilled out,
Eaten and decayed.
Even young gods decay.

Run
Little one,
Pump your untested legs to the edge
Of the world that you know.
Run away until
You can only run backwards.

You'll grow to hate the word "angry"
As much as the anger itself,
Will feel the hot sting of silences
Because they have names that are dead
To you.

The quiet conversations
Will be the worst because
You will not be righteous war,
No pyre will drown out your memories.
You will remember these,
And I am sorry.

Ball up your fists -
I have little more to tell you
But that you must only swing
When your eyes see a target.
You will miss,
And you will hit and regret it,
But you will never be
Less than a sailor -
Use your eyes
And you will never drown.

You were born without consent,
A bastard by choice
Because you would rather be illegitimate,
But the fertile valleys that have
Taken the iron from you,
Eaten child,
Did not know your forebears' sins.

You were not a lucky one,
But ghosts are stories,
Air in the mouths
Of the animated.