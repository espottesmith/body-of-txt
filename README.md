# body of .txt

The poetry of Evan Walter Clark Spotte-Smith.

The idea behind this repository is not only to catalog poems, but to catalog a history of their writing, from inception to the steady-state. Git allows me to record in snapshots all of the major edits, additions, and deletions that are made, and to let those be analyzed.